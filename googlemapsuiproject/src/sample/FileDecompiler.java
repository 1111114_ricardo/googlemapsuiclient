package sample;

import com.lynden.gmapsfx.javascript.object.LatLong;
import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class FileDecompiler {

    public static List<LatLong> decompileFile(@NotNull File file) throws IOException {
        final ArrayList<LatLong> coordinates = new ArrayList<>();
        Stream<String> lines = Files.lines(Paths.get(file.getPath()));
        lines.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                LatLong latLong = handleString(s);
                if(latLong != null){
                    coordinates.add(latLong);
                }
            }
        });
//        coordinates.add(new LatLong(47.6, -122.3));
//        handleString("47.6S 122.3W");
        return coordinates;
    }

    private static LatLong handleString(String s) {
        if(s.isEmpty()) return null;

        char c = s.charAt(0);
        if (c >= '0' && c <= '9'){
            String[] coordinates = s.split(" ");
            char n = coordinates[0].charAt(coordinates[0].length()-1);
            coordinates[0] = coordinates[0].replace(n+"","");
            double cord1 = (n == 'S' || n == 'W') ? -Double.valueOf(coordinates[0]): Double.valueOf(coordinates[0]);

            n = coordinates[1].charAt(coordinates[1].length()-1);
            coordinates[1] = coordinates[1].replace(n+"","");
            double cord2 = (n == 'S' || n == 'W') ? -Double.valueOf(coordinates[1]): Double.valueOf(coordinates[1]);
            System.out.println(cord1 +" " +cord2);
            return new LatLong(cord1,cord2);
        }
        return null;
    }

}

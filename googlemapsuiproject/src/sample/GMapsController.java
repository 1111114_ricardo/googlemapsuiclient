package sample;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.List;

public class GMapsController implements MapComponentInitializedListener {

    GoogleMapView mapView;
    GoogleMap map;
    private List<Marker> currentMarkers;

    public GMapsController(){
        //Create the JavaFX component and set this as a listener so we know when
        //the map has been initialized, at which point we can then begin manipulating it.
        mapView = new GoogleMapView();
        mapView.addMapInializedListener(this);
        currentMarkers = new ArrayList<>();
    }

    @Override
    public void mapInitialized() {
        //Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(47.6097, -122.3331))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);

        map = mapView.createMap(mapOptions);

    }

    public void reloadMarkers(@NotNull List<LatLong> coordinates) {
        for(Marker marker : currentMarkers){
            map.removeMarker(marker);
        }

        currentMarkers.clear();
        for(LatLong latLong : coordinates){
            Marker marker = new Marker( new MarkerOptions().position(latLong).visible(true) );
            currentMarkers.add(marker);
            map.addMarker(marker);
        }
    }
}

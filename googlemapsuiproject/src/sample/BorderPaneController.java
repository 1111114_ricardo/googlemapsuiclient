package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class BorderPaneController extends BorderPane{
    private Stage stage;
    private GMapsController gMapsController;
    private File currentFile;

    public BorderPaneController(final Stage stage){
        this.stage = stage;
        this.gMapsController = new GMapsController();
        this.setTop(addHBox());
        this.setCenter(gMapsController.mapView);
    }

    public HBox addHBox() {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
//        hbox.setStyle("-fx-background-color: #336699;");

        Button buttonCurrent = new Button("Choose File");
        buttonCurrent.setPrefSize(100, 20);
        buttonCurrent.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                chooseFile();
            }
        });

        hbox.getChildren().addAll(buttonCurrent);

        return hbox;
    }

    private void chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showOpenDialog(stage);
        if(file != null){
            this.currentFile = file;
            try {
                gMapsController.reloadMarkers(FileDecompiler.decompileFile(currentFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
